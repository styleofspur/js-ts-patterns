import path  from 'path';
import yargs from 'yargs';

const options = yargs().argv;
options.env = process.env.NODE_ENV || 'dev';

const paths = {
  src:  path.resolve('./src'),
  test: path.resolve('./test'),
  dist: path.resolve('./dist')
};

const loaders = {
  ts: {
    test: /\.ts$/, loader: 'ts'
  },
  js: {
    test: /\.js$/, loader: 'babel'
  },

};

// CONFIGS

const baseConfig = {
  watch: !!(options.watch || options.w),
  cache: true,
  stats: {
    errors:       true,
    warnings:     true,
    chunks:       false,
    assets:       false,
    colors:       true,
    version:      false,
    hash:         true,
    timings:      false,
    chunkModules: false,
    entryPoints:  false,
    chunkOrigins: false,
    cached:       false,
    cachedAssets: false,
    reasons:      false,
    usedExports:  false,
    children:     false,
    source:       false,
    modules:      false
  }
};

const baseEntryConfig = {
  output: {
    filename: `${paths.dist}/ts/bundle.js`
  },
  devtool: 'source-map',
  resolve: {
    modulesDirectories: [ 'node_modules' ],
  },
};

const entries = [
  // ts
  {
    context: path.join(paths.src, 'ts'),
    entry: './index.ts',
    module: {
      loaders: [
        loaders.ts
      ]
    }
  },
  // js
  {
    context: path.join(paths.src, 'js'),
    entry: './index.js',
    module: {
      loaders: [
        loaders.js
      ]
    }
  }
];

let mergedEntriesConfig = [];
entries.forEach(entryConfig => {
  mergedEntriesConfig.push(Object.assign({}, baseEntryConfig, entryConfig));
});

export default Object.assign([], baseConfig, mergedEntriesConfig);
