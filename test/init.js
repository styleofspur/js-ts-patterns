/* global env */
const options = process.env;

setLogLevel();

// functions declarations

function setLogLevel() {
    console.debug = options.LOG_LEVEL && options.LOG_LEVEL.toLowerCase() === 'fatal' ?  () => {} : msg => console.log(msg);
}