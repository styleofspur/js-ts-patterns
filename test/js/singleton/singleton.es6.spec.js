let Singleton;

describe('ES6.Singleton', () => {

  before(done => {

    // add .destroy() method in order to reset instance (for testing purposes only!)
    try {
      const fs           = require('fs');
      const babel        = require('babel-core');
      const filePath     = './src/js/singleton/singleton.es6.js';
      const fileContents = fs.readFileSync(filePath, 'utf-8');
      const babelOutput  = babel.transform(fileContents, {
        presets: ['es2015']
      });
      Singleton = eval(
          babelOutput.code.replace(
              'return Singleton;',
              'Singleton.destroy = function() { _instance = null; }\n\n  return Singleton;'
          )
      );
      done();
    } catch (err) {
      done(err);
    }
  });

  // reset Singleton after each "it" block
  afterEach(() => {
      Singleton.destroy();
  });

  it('non-invokable constructor', () => {
    try {
      new Singleton();
    } catch (err) {
      console.assert(err.message === 'ES6.Singleton constructor is private');
    }
  });

  it('only 1 instance', () => {
    const instance       = Singleton.getInstance();
    const secondInstance = Singleton.getInstance();

    console.assert(instance === secondInstance /*, errorMessage */);
  });

  it('instanceof', () => {
    const instance       = Singleton.getInstance();
    const secondInstance = Singleton.getInstance();

    console.assert(instance       instanceof Singleton);
    console.assert(secondInstance instanceof Singleton);
  });

  it('instance.constructor', () => {
    const instance = Singleton.getInstance();

    console.assert(instance.constructor === Singleton);
  });

  it('change', () => {
    const instance       = Singleton.getInstance();
    const secondInstance = Singleton.getInstance();

    instance.writableAndEnumerableProp = 1;

    console.assert(instance.writableAndEnumerableProp === 1);
    console.assert(instance.writableAndEnumerableProp === secondInstance.writableAndEnumerableProp);
  });

  it('add new member', () => {
    const instance       = Singleton.getInstance();
    const secondInstance = Singleton.getInstance();
    instance.newProp = 1;

    console.assert(instance.newProp === 1);
    console.assert(instance.newProp === secondInstance.newProp);
  });

  it('delete a member', () => {
    const instance       = Singleton.getInstance();
    const secondInstance = Singleton.getInstance();
    delete instance.deletableProp;

    console.assert(secondInstance.deletableProp === undefined);

    try {
      delete instance.writableAndEnumerableProp;
    } catch (err) {
      console.assert(err.message.match(/^Cannot delete property 'writableAndEnumerableProp'/));
    }
  });

  it('freeze=true', () => {
        const instance = Singleton.getInstance({freeze: true});

        try {
            instance.writableAndEnumerableProp = 1;
        } catch (err) {
            console.assert(err.message.match(/^Cannot assign to read only property 'writableAndEnumerableProp' of object/));
        }

        try {
            instance.newProp = 1;
        } catch (err) {
            console.assert(err.message.match(/^Can't add property newProp, object is not extensible/));
        }
    });

  it('freeze=false', () => {
        const instance = Singleton.getInstance({freeze: false});
        instance.writableAndEnumerableProp = 1;
        instance.newProp                   = 2;

        console.assert(instance.writableAndEnumerableProp === 1);
        console.assert(instance.newProp === 2);
    });

  it('deserialized instance inequality', () => {
    const instance     = Singleton.getInstance();
    const serialized   = JSON.stringify(instance);
    const deserialized = JSON.parse(serialized);

    console.assert(Object.keys(deserialized).length === 1);
    console.assert(Object.keys(instance).length !== Object.keys(deserialized).length);
    for (const prop in deserialized) {
      console.assert(instance.hasOwnProperty(prop) === false);
    }
  });

  it('Object.assign() prevention', () => {
    const instance     = Singleton.getInstance();
    const instanceKeys = Object.keys(instance);
    const copy         = Object.assign({}, instance);
    const copyKeys     = Object.keys(copy);

    console.assert(copyKeys.length === instanceKeys.length);
    console.assert(copyKeys.length === 2);

    console.assert(copyKeys.includes('writableAndEnumerableProp'));
    console.assert(copyKeys.includes('deletableProp'));

    console.assert(!instanceKeys.includes('nonWritableAndNonEnumerableProp'));
    console.assert(!copyKeys.includes('nonWritableAndNonEnumerableProp'));

    console.assert(instance.nonWritableAndNonEnumerableProp !== undefined);
    console.assert(copy.nonWritableAndNonEnumerableProp === undefined);
  });

});