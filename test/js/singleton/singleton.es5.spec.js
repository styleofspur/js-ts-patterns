var Singleton;

describe('ES5.Singleton', () => {

  before(function(done) {

    // add .destroy() method in order to reset instance (for testing purposes only!)
    try {
      var fs           = require('fs');
      var filePath     = './src/js/singleton/singleton.es5.js';
      var fileContents = fs.readFileSync(filePath, 'utf8');
      var sources      = fileContents.replace(
        'return Singleton;',
        'Singleton.destroy = function() { _instance = null; }\n\n  return Singleton;'
      );
      Singleton = eval(sources);
      done();
    } catch (err) {
      done(err);
    }
  });

  // reset Singleton after each "it" block
  afterEach(function() {
    Singleton.destroy();
  });

  it('non-invokable constructor', () => {
    try {
      new Singleton();
    } catch (err) {
      console.assert(err.message === 'ES5.Singleton constructor is private');
    }
  });

  it('only 1 instance', () => {
    var instance       = Singleton.getInstance();
    var secondInstance = Singleton.getInstance();

    console.assert(instance === secondInstance /*, errorMessage */);
  });

  it('instanceof', () => {
    var instance       = Singleton.getInstance();
    var secondInstance = Singleton.getInstance();

    console.assert(instance instanceof Singleton);
    console.assert(secondInstance instanceof Singleton);
  });

  it('instance.constructor', () => {
    var instance = Singleton.getInstance();

    console.assert(instance.constructor === Singleton);
  });

  it('change', () => {
    var instance       = Singleton.getInstance();
    var secondInstance = Singleton.getInstance();

    instance.writableAndEnumerableProp = 1;

    console.assert(instance.writableAndEnumerableProp === 1);
    console.assert(instance.writableAndEnumerableProp === secondInstance.writableAndEnumerableProp);
  });

  it('add new member', () => {
    var instance       = Singleton.getInstance();
    var secondInstance = Singleton.getInstance();

    instance.newProp = 1;

    console.assert(instance.newProp === 1);
    console.assert(instance.newProp === secondInstance.newProp);
  });

  it('delete a member', () => {
    var instance       = Singleton.getInstance();
    var secondInstance = Singleton.getInstance();

    delete instance.deletableProp;
    console.assert(secondInstance.deletableProp === undefined);

    try {
      delete instance.writableAndEnumerableProp;
    } catch (err) {
      console.assert(err.message.match(/^Cannot delete property 'writableAndEnumerableProp'/));
    }
  });

  it('freeze=true', () => {
    var instance = Singleton.getInstance({freeze: true});

    try {
      instance.writableAndEnumerableProp = 1;
    } catch (err) {
      console.assert(err.message.match(/^Cannot assign to read only property 'writableAndEnumerableProp' of object/));
    }

    try {
      instance.newProp = 1;
    } catch (err) {
      console.assert(err.message.match(/^Can't add property newProp, object is not extensible/));
    }
  });

  it('freeze=false', () => {
    var instance = Singleton.getInstance({freeze: false});
    instance.writableAndEnumerableProp = 1;
    instance.newProp                   = 2;

    console.assert(instance.writableAndEnumerableProp === 1);
    console.assert(instance.newProp === 2);
  });

  it('deserialized instance inequality', () => {
    var instance     = Singleton.getInstance();
    var serialized   = JSON.stringify(instance);
    var deserialized = JSON.parse(serialized);

    console.assert(Object.keys(deserialized).length === 1);
    console.assert(Object.keys(instance).length !== Object.keys(deserialized).length);
    for (var prop in deserialized) {
      console.assert(instance.hasOwnProperty(prop) === false);
    }
  });

  it('Object.assign() prevention', () => {
    var instance     = Singleton.getInstance();
    var instanceKeys = Object.keys(instance);
    var copy         = Object.assign({}, instance);
    var copyKeys     = Object.keys(copy);

    console.assert(copyKeys.length === instanceKeys.length);
    console.assert(copyKeys.length === 2);

    console.assert(copyKeys.includes('writableAndEnumerableProp'));
    console.assert(copyKeys.includes('deletableProp'));

    console.assert(!instanceKeys.includes('nonWritableAndNonEnumerableProp'));
    console.assert(!copyKeys.includes('nonWritableAndNonEnumerableProp'));

    console.assert(instance.nonWritableAndNonEnumerableProp !== undefined);
    console.assert(copy.nonWritableAndNonEnumerableProp === undefined);
  });

});