import 'reflect-metadata';
import Singleton from '../../src/ts/singleton/singleton';

describe('Singleton', () => {

    afterEach(() => {
        Object.defineProperty(
            Singleton,
            '_instance',
            Object.assign({}, Object.getOwnPropertyDescriptor(Singleton, '_instance'), {value: null})
        );
    });

    it('only 1 instance', () => {
        const instance: Singleton       = Singleton.getInstance();
        const secondInstance: Singleton = Singleton.getInstance();

        console.assert(instance === secondInstance);
    });

    it('instanceof', () => {
        const instance: Singleton       = Singleton.getInstance();
        const secondInstance: Singleton = Singleton.getInstance();

        console.assert(instance       instanceof Singleton);
        console.assert(secondInstance instanceof Singleton);
    });

    it('instance.constructor', () => {
      const instance: Singleton = Singleton.getInstance();

      console.assert(instance.constructor === Singleton);
    });

    it('change', () => {
        const instance: Singleton       = Singleton.getInstance();
        const secondInstance: Singleton = Singleton.getInstance();

        instance.writableAndEnumerableProp = 1;

        console.assert(instance.writableAndEnumerableProp === 1);
        console.assert(instance.writableAndEnumerableProp === secondInstance.writableAndEnumerableProp);
    });

    it('add new member', () => {
        const instance: Singleton       = Singleton.getInstance();
        const secondInstance: Singleton = Singleton.getInstance();
        instance.newProps.foo = 1;

        console.assert(instance.newProps.foo === 1);
        console.assert(instance.newProps.foo === secondInstance.newProps.foo);
    });

    it('delete a member', () => {
        const instance: Singleton       = Singleton.getInstance();
        const secondInstance: Singleton = Singleton.getInstance();
        delete instance.deletableProp;

        console.assert(secondInstance.deletableProp === undefined);

        try {
            delete instance.writableAndEnumerableProp;
        } catch (err) {
            console.assert(err.message.match(/^Cannot delete property 'writableAndEnumerableProp'/));
        }
    });

    it('deserialized instance inequality', () => {
        const instance: Singleton     = Singleton.getInstance();
        const serialized: string      = JSON.stringify(instance);
        const deserialized: Singleton = JSON.parse(serialized);

        console.assert(Object.keys(deserialized).length === 1);
        console.assert(Object.keys(instance).length !== Object.keys(deserialized).length);
        for (const prop in deserialized) {
            console.assert(instance.hasOwnProperty(prop) === false);
        }
    });

    it('Object.assign() prevention', () => {
        // @todo: add property descriptor
        const instance: Singleton         = Singleton.getInstance();
        const instanceKeys: Array<string> = Object.keys(instance);
        const copy: Singleton             = Object.assign({}, instance);
        const copyKeys: Array<string>     = Object.keys(copy);

        console.assert(copyKeys.length === instanceKeys.length);
        console.assert(copyKeys.length === 4);

        console.assert(copyKeys.indexOf('writableAndEnumerableProp') >= 0);
        console.assert(copyKeys.indexOf('deletableProp') >= 0);

        // console.assert(instanceKeys.indexOf('nonWritableAndNonEnumerableProp') === -1);
        // console.assert(copyKeys.indexOf('nonWritableAndNonEnumerableProp') === -1);

        console.assert(instance.nonWritableAndNonEnumerableProp !== undefined);
        // console.assert(copy.nonWritableAndNonEnumerableProp === undefined);
    });

});