import { configurable, writable, enumerable } from '../methodDecorators';

export default class Singleton {

    public nonWritableAndNonEnumerableProp: any = 'nonWritableAndNonEnumerableProp';
    public writableAndEnumerableProp: any = 'writableAndEnumerableProp';
    public deletableProp: any = 'deletableProp';
    public newProps: any = {};

    /**
     * @property {Singleton} _instance
     * @private
     */
    private static _instance: Singleton;

    /**
     * @constructor
     * @returns {Singleton}
     */
    private constructor() {
        return this;
    }

    @configurable(false)
    @writable(false)
    @enumerable(false)
    static getInstance() : Singleton {
        if (Singleton._instance) {
            return Singleton._instance;
        }

        Singleton._instance = new Singleton();
        return Singleton._instance;
    }

    public toJSON() : {} {
        return {randomProp: 1};
    }
}