export function configurable(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) : void {
    descriptor.configurable = value;
  };

}

export function writable(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) : void {
    descriptor.writable = value;
  };
}

export function enumerable(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) : void {
    descriptor.enumerable = value;
  };
}