'use strict';

var Singleton = (function() {
  var _instance            = null;
  var _callableConstructor = false;
  var _instanceMembers     = [
    {
      name: 'deletableProp',
      def:  {
        value:        'deletable data',
        configurable: true, // deletable, re-configurable
        enumerable:   true, // for...in & Object.assign() available
        writable:     true
      }
    },{
      name: 'writableAndEnumerableProp',
      def:  {
        value:        'writable and enumerable data',
        configurable: false, // non-deletable, non-configurable
        enumerable:   true,  // for...in & Object.assign() available
        writable:     true
      }
    }, {
      name: 'nonWritableAndNonEnumerableProp',
      def:  {
        value:        'non-writable and non-enumerable data',
        configurable: false, // locks everything
        enumerable:   false, // for...in & Object.assign() unavailable
        writable:     false  // readonly
      }
    }, {
      name: 'toJSON',
      def: {
        value: function() {
            return {deSerializedProp: 'test'};
        },
        configurable: false, // locks everything
        enumerable:   false, // for...in & Object.assign() unavailable
        writable:     false  // readonly
      }
    }
  ];

  /**
   * Singleton class constructor.
   *
   * @returns {*}
   * @constructor
   */
  function Singleton() {

    if (!_callableConstructor) {
      throw new Error(this + ' constructor is private');
    }

    if (_instance) {
      console.debug(this + ' returning instance');
      return _instance;
    }

    console.debug(this + ' creating instance');
  }

  /**
   * Object to string converter.
   * @returns {string}
   */
  Singleton.prototype.toString = function() {
    return 'ES5.Singleton';
  };

  /**
   * Main entry point.
   * Returns new/created instance of Singleton class.
   *
   * @param options
   * @returns {Singleton}
   */
  Singleton.getInstance = function(options) {
    if (_instance) return _instance;

    _callableConstructor = true; // make constructor invokable
    _instance = new Singleton(); // create new/get existing instance
    _callableConstructor = false; // make constructor non-invokable as it's by default

    // define instance members (properties & methods with their settings)

    _instanceMembers.forEach(function(prop) {
      Object.defineProperty(_instance, prop.name, prop.def);
    });

    // apply options
    console.debug('ES5.Singleton options', options);
    if (options && options.freeze) {
      Object.freeze(_instance);
    }

    return _instance;
  };

  return Singleton;

})();

module.exports = Singleton;
