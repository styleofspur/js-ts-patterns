export default (() => {
  let _instance            = null;
  let _callableConstructor = false;

  class Singleton {

      /**
       *
       * @static
       * @param options
       * @returns {Singleton}
       */
      static getInstance(options = {}) {
          if (_instance) return _instance;

          _callableConstructor = true;
          _instance = new Singleton();
          _callableConstructor = false;

          Object.defineProperties(_instance, {
              deletableProp:                   {
                  value:        'deletable data',
                  configurable: true, // deletable, re-configurable
                  enumerable:   true, // for...in & Object.assign() available
                  writable:     true
              },
              writableAndEnumerableProp:       {
                  value:        'writable and enumerable data',
                  configurable: false, // non-deletable, non-configurable
                  enumerable:   true,  // for...in & Object.assign() available
                  writable:     true
              },
              nonWritableAndNonEnumerableProp: {
                  value:        'non-writable and non-enumerable data',
                  configurable: false, // locks everything
                  enumerable:   false, // for...in & Object.assign() unavailable
                  writable:     false  // readonly
              },
              toJSON: {
                  value: function() {
                      return {deSerializedProp: 'test'};
                  },
                  configurable: false, // locks everything
                  enumerable:   false, // for...in & Object.assign() unavailable
                  writable:     false  // readonly
              }
          });

          console.debug(`ES6.Singleton options`, options);
          if (options.freeze) {
            Object.freeze(_instance);
          }

          return _instance;
      }

      /**
       * @constructor
       */
      constructor() {
          if (!_callableConstructor) {
              throw new Error(`${this} constructor is private`);
          }

          if (_instance) {
              console.debug(`${this} returning instance`);
              return _instance;
          }

          console.debug(`${this} creating instance`);
      }

      /**
       * @public
       * @returns {string}
       */
      toString() {
        return 'ES6.Singleton';
      }

  }

  return Singleton;
})();